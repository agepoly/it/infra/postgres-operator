Postgres Operator
=================

Postgres Operator by Zalando used to manage Agepoly's main cluster.

This chart is a wrapper for two upstream charts :
- (Postgres Operator)[https://github.com/zalando/postgres-operator/tree/master/charts/postgres-operator]
- (Postgres Operator UI)[https://github.com/zalando/postgres-operator/tree/master/charts/postgres-operator-ui]

DB's are defined in ./chart/template/production-cluster.yml
database name should be in the form NAMESPACE_DBNAME
user should be in the form NAMESPACE.DBNAME
credentials will be provided in a secret named (in namespace NAMESPACE)
NAMESPACE.DBNAME.agepoly-production.credentials.postgresql.acid.zalan.do
key are "password" and "username"
chart are configured to use automatically dbs when DBNAME = NAMESPACE and values useDb is set
